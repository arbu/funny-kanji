abstract class ConfigKeys {
  static const String readOutLoud = 'read_out_loud';
  static const String playSoundEffects = 'play_sound_effects';
}
